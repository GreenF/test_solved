// your code goes below
$(document).ready(function(){

    var firstname = $("#first-name");
    var labelfirstname = $('label[for="first-name"]');
    if(firstname.val()===""){
            labelfirstname.hide().removeClass('label-no-hidden');
            firstname.attr("placeholder", "First Name");
        }
                
        firstname.keyup(function() {
            if(firstname.val()===""){
                labelfirstname.hide().removeClass('label-no-hidden');
                firstname.attr("placeholder", "First Name");
            }
            else{
                labelfirstname.show().addClass('label-no-hidden');
                firstname.attr("value", "").addClass('label-no-hidden');
            }
    });
    
    var lastname = $("#last-name");
    var labellastname = $('label[for="last-name"]');
    if(lastname.val()===""){
            labellastname.hide().removeClass('label-no-hidden');
            lastname.attr("placeholder", "Last Name").removeClass('field-no-hidden');
        }
        else{
            labellastname.show().addClass('label-no-hidden');
            lastname.addClass('field-no-hidden');
        }
                
        lastname.keyup(function() {
            if(lastname.val()===""){
                labellastname.hide().removeClass('label-no-hidden');
                lastname.attr("placeholder", "Credit Card Name").removeClass('field-no-hidden');
            }
            else{
                labellastname.show().addClass('label-no-hidden');
                lastname.attr("value", "").addClass('field-no-hidden');
            }
    });
    
    var creditcard = $("#credit-card");
    var labelcreditcard = $('label[for="first-name"]');
    if(creditcard.val()===""){
            labelcreditcard.hide().removeClass('label-no-hidden');
            creditcard.attr("placeholder", "Credit Card").removeClass('field-no-hidden');
        }
        else{
            labelcreditcard.show().addClass('label-no-hidden');
        }
                
        creditcard.keyup(function() {
            if(creditcard.val()===""){
                labelcreditcard.hide().removeClass('label-no-hidden');
                creditcard.attr("placeholder", "First Name");
            }
            else{
                labelcreditcard.show().addClass('label-no-hidden');
                creditcard.attr("value", "");
            }
    });
    
    var shline1 = $("#shipping-line-1");
    var labelshline1 = $('label[for="shipping-line-1"]');
    if(shline1.val()===""){
            labelshline1.hide().removeClass('label-no-hidden');
            shline1.attr("placeholder", "First Name").removeClass('field-no-hidden');
        }
                
        shline1.keyup(function() {
            if(shline1.val()===""){
                labelshline1.hide().removeClass('label-no-hidden');
                shline1.attr("placeholder", "address").removeClass('field-no-hidden');
            }
            else{
                labelshline1.show().addClass('label-no-hidden');
                shline1.attr("value", "").addClass('field-no-hidden');
            }
    });
});