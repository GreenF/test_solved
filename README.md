# Enhanced placeholder fields

The aim of this exercise is first to build modern looking field with a intelligent placeholder.

The second objective is to handle the visual version of the credit card field.

A last objectives, are the bonus. Make sure that everything else, placeholder and credit card are finished before trying the bonus.

Global objective:

![enhanced placeholder example](./img/form.png)

First field show the version of the field with data and the others are empty (`value=""`).

Only additions in the `js/scripts.js` and `style/styles.css` are allowed. Some styling is already available.

jQuery 1.11.3 is also provided.

## Expected behaviours

### Placeholder

Starting with the following markup:

```html
<div class="beautiful-field field-group">
    <label class="label" for="first-name">First Name</label>
    <input class="field" id="first-name" value="Steven" autocomplete="off" type="text" />
</div>
```

build an intelligent field with a moving label.

`.beautiful-field` is the class to apply the behaviour on.
`label.label` and `input.field` are mandatory in `.beautiful-field`.

When the field is empty, the label should be by default "inside" of the input, like if it was a html5 placeholder.

When the user types something in the field the label is moved on top of the field, like in the first field in the screenshot.

#### Success criteria

1. Styles are correct (css)
2. Behaviour are as expected (css and js)
3. [bonus] (do at the end) there is an animation to move the label up and down
4. [bonus] (do at the end) create a jQuery plugin for this behaviour

### Credit card field

The aim is to handle the credit card field in order to insert the space between each block of 4 characters and filters out unwanted characters (alphabetical and numerical characters are allowed).

The behaviour should be applied *as the user types* and should look like on the following screenshot:

![credit card nice field](img/credit-card.png)

#### Success criteria

1. spaces are added between all 4 groups of characters
2. they are added as the user is typing
3. it does not break the "enhanced placeholder"
4. backspace is supported
5. [bonus] (do at the end) Normal operations like moving around with arrows or editing with mouse should still be possible